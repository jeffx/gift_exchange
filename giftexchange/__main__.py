import argparse
import random
import sys

from person import Person
from helpers import convert_string_to_date

config = {"age_range": 5}


def create_participant(data):
    split_part = data.split(";")
    kwargs = {"name": split_part[0],
              "date_of_birth": convert_string_to_date(split_part[1]),
              "sex": split_part[2],
              "address": split_part[3]
              }
    p = Person(**kwargs)
    return p


def load_participants_from_file(filename):
    participants = []
    with open(filename, "r") as participants_file:
        for participant in participants_file:
            participants.append(create_participant(participant))
    return participants


def main():
    pairings = []
    command_args = process_command_line(sys.argv[1:])
    participants = load_participants_from_file(command_args["file"])
    participants_giftor = participants.copy()
    participants_giftee = participants.copy()
    control_break = False
    while not control_break:
        if len(participants_giftor) <= 0:
            break
        else:
            match = False
            giftee_copy = participants_giftee.copy()
            giftor = random.choice(participants_giftor)
            while not match:
                if len(giftee_copy) <= 0:
                    print("no match available")
                    control_break = True
                    break
                else:
                    giftee = random.choice(giftee_copy)
                    if giftor.address == giftee.address:
                        giftee_copy.remove(giftee)
                        continue
                    if giftee.age not in range(giftor.age-5, giftor.age+5):
                        giftee_copy.remove(giftee)
                        continue
                    else:
                        print(giftor.name)
                        print(giftee.name)
                        print(f"{giftor.age}:{giftee.age}")
                        match = True
            if match:
                pairings.append((giftor, giftee))
                participants_giftee.remove(giftee)
                participants_giftor.remove(giftor)
            else:
                print("No match 2")
                participants_giftee.remove(giftee)
                participants_giftor.remove(giftor)

                
    print(pairings)


            






def process_command_line(args):
    parser = argparse.ArgumentParser(description="Brett's Longtime No Hear app")
    parser.add_argument("file", help="Path to participants")
    args = parser.parse_args(args)
    return vars(args)


if __name__ == "__main__":
    main()
