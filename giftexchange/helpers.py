from datetime import datetime


def convert_string_to_date(date_string):
    datetime_object = datetime.strptime(date_string, "%m/%d/%Y")
    return datetime_object
