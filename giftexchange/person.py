from datetime import date
class Person:
    def __init__(self, name=None, date_of_birth=None, sex=None, address=None):
        self.name = name
        self.date_of_birth = date_of_birth
        self.sex = sex
        self.address = address


    @property
    def age(self):
        today = date.today()
        born = self.date_of_birth
        return today.year - born.year - ((today.month, today.day) < (born.month, born.day))
